import {MemoryRouter} from "react-router-dom";
import {StoresProvider, stores} from "stores";
import {render, screen, waitFor} from "@testing-library/react";
import {rest} from "msw"
import {setupServer} from "msw/node"
import App from "App";
import React, {FC} from "react";
import currenciesMock from "mocks/currenciesMock";
import getApiUrl, {APIMethods} from "api/getApiUrl";
import ratesMock from "mocks/ratesMock";

const urlCurrencies = getApiUrl(APIMethods.GET_CURRENCIES_LIST).split("?")[0];
const urlRates = getApiUrl(APIMethods.GET_RATES).split("?")[0];
let successResponse = true;
const server = setupServer(
  rest.get(urlCurrencies, (req, res, ctx) => {
    return successResponse ?
      res(ctx.json(currenciesMock)) :
      res(
        ctx.status(500, "Network error"),
        ctx.body("")
      );
  }),
  rest.get(urlRates, (req, res, ctx) => {
    return res(ctx.json(ratesMock));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

const Application: FC<{initialEntries: string[]}> = ({initialEntries}) => <div data-testid="app">
  <StoresProvider value={stores}>
    <MemoryRouter initialEntries={initialEntries}>
      <App />
    </MemoryRouter>
  </StoresProvider>
</div>;

describe("App", () => {
  test("should render accounts with default settings", async () => {

    const {unmount} = render(<Application initialEntries={["/accounts"]}/>);

    await waitFor(() => screen.getByRole("tablist"), {timeout: 1000});
    const app = screen.getByTestId("app");
    expect(app).toMatchSnapshot();

    unmount();
  });

  test("should display error", async () => {
    successResponse = false;
    stores.currencyStore.setCurrencies(null);

    const {unmount} = render(<Application initialEntries={["/accounts"]}/>);

    await waitFor(() => screen.getByRole("alert"), {timeout: 1000});
    const app = screen.getByTestId("app");
    expect(app).toMatchSnapshot();
    unmount();
  });

  test("should render exchange with default settings", async () => {
    successResponse = true;
    const {unmount} = render(<Application initialEntries={["/exchange"]}/>);

    await waitFor(() => screen.getByRole("separator"), {timeout: 1000});
    const app = screen.getByTestId("app");
    expect(app).toMatchSnapshot();
    unmount();
  });


});

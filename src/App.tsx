import { Alert } from "antd";
import { observer } from "mobx-react-lite";
import {useHistory} from "react-router-dom";
import Loading from "components/Loading";
import NavBar from "components/NavBar";
import Navigation from "navigation";
import Page from "components/Page";
import React, { FC, useEffect } from "react";
import getPath from "utils/getPath";
import usePageCodeFromUrl from "hooks/usePageCodeFromUrl";
import useStores from "hooks/useStores";

const App: FC = observer(() => {
  const history = useHistory();
  const { currencyStore } = useStores();
  const pageCode = usePageCodeFromUrl();

  useEffect(() => {
    void currencyStore.getCurrenciesList();
  }, [currencyStore]);

  return (
    <Page>
      {currencyStore.isPending && <Loading size={40} />}
      {currencyStore.isError && (
        <Alert showIcon message={currencyStore.error} type="error" />
      )}
      {currencyStore.isReady && !currencyStore.isError && (
        <>
          <NavBar
            activeKey={pageCode}
            onChange={(key) => {
              history.push(getPath(key));
            }} />
          <Navigation />
        </>
      )}
    </Page>
  );
});

export default App;

import { CurrenciesList } from "types";
import getApiUrl, { APIMethods } from "api/getApiUrl";

const fetchCurrenciesList = async (): Promise<CurrenciesList> => {
  const url = getApiUrl(APIMethods.GET_CURRENCIES_LIST);
  const response = await fetch(url);
  return response.json();
};

export default fetchCurrenciesList;

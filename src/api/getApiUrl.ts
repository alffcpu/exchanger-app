import { API_KEY } from "config";

export enum APIMethods {
  GET_CURRENCIES_LIST,
  GET_RATES,
}

export const apiMethods: Record<APIMethods, string> = {
  [APIMethods.GET_CURRENCIES_LIST]: "currencies",
  [APIMethods.GET_RATES]: `latest`,
};

const getApiUrl = (apiMethod: APIMethods): string =>
  `https://openexchangerates.org/api/${apiMethods[apiMethod]}.json?app_id=${API_KEY}`;

export default getApiUrl;

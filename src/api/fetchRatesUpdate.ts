import { Rates } from "types";
import { fxBase, fxRates } from "utils/money";
import sleep from "utils/sleep";

const fetchRatesUpdate = async () => {
  const delay = randomNumber(500, 1500);
  await sleep(delay);
  const rates = Object.entries(fxRates()).reduce<Rates>(
    (result, [code, rate]) => {
      const diff = randomNumber(-0.1, 0.11);
      return {
        ...result,
        [code]: code !== "USD" ? rate + diff : 1,
      };
    },
    {}
  );
  return { base: fxBase(), rates };
};

const randomNumber = (min: number, max: number) => {
  return Math.random() * (max - min) + min;
};

export default fetchRatesUpdate;

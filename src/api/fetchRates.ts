import { Rates, RatesResponse } from "types";
import getApiUrl, { APIMethods } from "api/getApiUrl";

const fetchRates = async (): Promise<{ base: string; rates: Rates }> => {
  const url = getApiUrl(APIMethods.GET_RATES);
  const response = await fetch(url);
  const { base, rates }: RatesResponse = await response.json();
  return { base, rates };
};

export default fetchRates;

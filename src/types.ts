export type CurrenciesList = Record<string, string>;
export type AccountsList = Record<string, number>;

export type CurrencyOptionItem = {
  value: string;
  title: string;
  disabled?: boolean;
};

export enum ExchangeDirection {
  FROM_TO,
  TO_FROM,
}

export type Rates = Record<string, number>;

export type RatesResponse = {
  disclaimer: string;
  license: string;
  timestamp: number;
  base: string;
  rates: Rates;
};

export enum SortOrder {
  NAME,
  AMOUNT,
}

export enum SortDir {
  ASC,
  DESC,
}

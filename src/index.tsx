import "antd/dist/antd.css";
import { BrowserRouter } from "react-router-dom";
import { StoresProvider, stores } from "stores";
import App from "App";
import React from "react";
import ReactDOM from "react-dom";
import reportWebVitals from "./reportWebVitals";

ReactDOM.render(
  <React.StrictMode>
    <StoresProvider value={stores}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </StoresProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

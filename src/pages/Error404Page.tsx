import React, { FC } from "react";

const Error404Page: FC = () => {
  return <h3>Page not found!</h3>;
};

export default Error404Page;

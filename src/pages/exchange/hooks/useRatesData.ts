import { RATES_REFRESH_DELAY } from "config";
import { message } from "antd";
import { useCallback, useEffect, useRef } from "react";
import useStores from "hooks/useStores";

const useRatesData = () => {
  const { exchangeStore } = useStores();
  const delayId = useRef<any>(0);

  const refresh = useCallback(async () => {
    await exchangeStore.refresh();
    exchangeStore.recalculate();
  }, [exchangeStore]);

  const deferUpdate = useCallback(() => {
    clearTimeout(delayId.current);
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    delayId.current = setTimeout(async () => {
      try {
        await refresh();
      } catch (err) {
        message.error(`${err}`);
      } finally {
        deferUpdate();
      }
    }, RATES_REFRESH_DELAY);
  }, [refresh]);

  const ready = exchangeStore.ready;
  useEffect(() => {
    if (!ready) {
      refresh();
    } else if (ready && !delayId.current) {
      deferUpdate();
    }
  }, [deferUpdate, ready, refresh]);

  useEffect(() => {
    return () => {
      clearTimeout(delayId.current);
      exchangeStore.ready = false;
    }
  }, [exchangeStore]);
};

export default useRatesData;

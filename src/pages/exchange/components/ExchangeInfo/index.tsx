import { Alert, Tooltip } from "antd";
import { convertNumberToLongMoneyString } from "utils/getFloatFromString";
import { observer } from "mobx-react-lite";
import ErrorIcon from "pages/exchange/components/styled/ErrorIcon";
import ExchangeRow from "pages/exchange/components/styled/ExchangeRow";
import React, { FC } from "react";
import SyncIcon from "pages/exchange/components/styled/SyncIcon";
import format from "date-fns/format";
import useStores from "hooks/useStores";

const ExchangeInfo: FC = observer(() => {
  const { exchangeStore } = useStores();

  const error = exchangeStore.error;
  const [from, to] = exchangeStore.safePair;
  const updated = exchangeStore.rateUpdated;

  return from && to && exchangeStore.ready ? (
    <ExchangeRow $gutters="0 0 0 32px">
      <Alert
        message={
          <>
            Rate: 1 {from} ={" "}
            {convertNumberToLongMoneyString(exchangeStore.rate)} {to}
            <br />
            Cross rate: 1 {to} ={" "}
            {convertNumberToLongMoneyString(exchangeStore.crossRate)} {from}
            <br />
            {!!updated && (
              <>Updated: {format(updated, "dd.MM.yyyy HH:mm:ss")}</>
            )}
            {!!error && (
              <Tooltip placement="top" title={error}>
                <ErrorIcon />
              </Tooltip>
            )}
            {exchangeStore.pending && <SyncIcon spin />}
          </>
        }
        type={error ? "error" : "info"}
      />
    </ExchangeRow>
  ) : null;
});

export default ExchangeInfo;

import { CurrencyOptionItem } from "types";
import { Select, SelectProps } from "antd";
import CurrencyDropdownSelect from "pages/exchange/components/styled/CurrencyDropdownSelect";
import React, { FC } from "react";

const { Option } = Select;

interface CurrencyDropdownProps extends Omit<SelectProps<string>, "onChange"> {
  options: CurrencyOptionItem[];
  onChange: (value: string) => void;
}

const CurrencyDropdown: FC<CurrencyDropdownProps> = ({
  options,
  defaultValue,
  onChange,
}) => {
  return (
    <CurrencyDropdownSelect
      dropdownMatchSelectWidth
      defaultValue={defaultValue}
      onChange={(value) => onChange(`${value}`)}
      size="large"
    >
      {options.map(({ value, title, disabled }) => (
        <Option key={value} disabled={disabled} value={value}>
          {title}
        </Option>
      ))}
    </CurrencyDropdownSelect>
  );
};

export default CurrencyDropdown;

import { Button, Divider } from "antd";
import { observer } from "mobx-react-lite";
import React, { FC } from "react";
import SwapIcon from "pages/exchange/components/styled/SwapIcon";
import useStores from "hooks/useStores";

const Swap: FC = observer(() => {
  const { exchangeStore } = useStores();

  const fromCurrency = exchangeStore.from;
  const toCurrency = exchangeStore.to;

  return (
    <Divider orientation="left">
      <Button
        data-testid="swap-button"
        icon={<SwapIcon />}
        onClick={() => exchangeStore.updateCurrency(toCurrency, fromCurrency)}
        shape="circle"
        size="large"
      />
    </Divider>
  );
});

export default Swap;

import { Alert, InputNumber } from "antd";
import { ExchangeDirection } from "types";
import { MinusOutlined } from "@ant-design/icons";
import { convertNumberToMoneyString } from "utils/getFloatFromString";
import { observer } from "mobx-react-lite";
import Amount from "pages/exchange/components/styled/Amount";
import CurrencyDropdown from "pages/exchange/components/ExchangeForm/CurrencyDropdown";
import ExchangeRow from "pages/exchange/components/styled/ExchangeRow";
import Limit from "pages/exchange/components/styled/Limit";
import React, { FC } from "react";
import useCurrencyOptionsList from "hooks/useCurrencyOptionsList";
import useStores from "hooks/useStores";

const From: FC = observer(() => {
  const { accountStore, exchangeStore } = useStores();

  const [fromCurrency, toCurrency] = exchangeStore.safePair;
  const amountFrom = exchangeStore.amountFrom;
  const handleChangeAmount = exchangeStore.calculate;
  const handleFocusAmount = exchangeStore.setDirectionFromTo;
  const handleChangeCurrency = exchangeStore.setFrom;
  const minFrom = exchangeStore.minFrom;

  const fromValue = amountFrom.toFixed(2);
  const balanceFrom = fromCurrency
    ? accountStore.accounts[fromCurrency] ?? 0
    : 0;

  const optionsFrom = useCurrencyOptionsList(toCurrency);

  return (
    <ExchangeRow>
      <MinusOutlined />
      <Amount>
        <InputNumber<string>
          stringMode
          bordered={exchangeStore.direction === ExchangeDirection.FROM_TO}
          max={`${balanceFrom}`}
          min={`${minFrom}`}
          onChange={handleChangeAmount}
          onFocus={handleFocusAmount}
          size="large"
          step="0.01"
          value={fromValue}
        />
      </Amount>
      <CurrencyDropdown
        key={fromCurrency}
        defaultValue={fromCurrency}
        onChange={handleChangeCurrency}
        options={optionsFrom}
      />
      {minFrom <= balanceFrom ? (
        <Limit>
          {convertNumberToMoneyString(minFrom)} &ndash;{" "}
          {convertNumberToMoneyString(balanceFrom)}
          <span>{fromCurrency}</span>
        </Limit>
      ) : (
        <Alert
          showIcon
          message={`Insufficient funds: ${balanceFrom} ${fromCurrency}`}
          style={{ marginLeft: "16px" }}
          type="error"
        />
      )}
    </ExchangeRow>
  );
});

export default From;

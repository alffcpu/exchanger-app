import { ExchangeDirection } from "types";
import { InputNumber } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { convertNumberToMoney } from "utils/getFloatFromString";
import { observer } from "mobx-react-lite";
import Amount from "pages/exchange/components/styled/Amount";
import CurrencyDropdown from "pages/exchange/components/ExchangeForm/CurrencyDropdown";
import ExchangeRow from "pages/exchange/components/styled/ExchangeRow";
import Limit from "pages/exchange/components/styled/Limit";
import React, { FC } from "react";
import useCurrencyOptionsList from "hooks/useCurrencyOptionsList";
import useStores from "hooks/useStores";

const To: FC = observer(() => {
  const { accountStore, exchangeStore } = useStores();

  const [fromCurrency, toCurrency] = exchangeStore.safePair;
  const amountTo = exchangeStore.amountTo;
  const handleChangeAmount = exchangeStore.calculate;
  const handleFocusAmount = exchangeStore.setDirectionToFrom;
  const handleChangeCurrency = exchangeStore.setTo;

  const optionsTo = useCurrencyOptionsList(fromCurrency);

  const toValue = amountTo.toFixed(2);
  const balanceTo = toCurrency ? accountStore.accounts[toCurrency] ?? 0 : 0;

  return (
    <ExchangeRow>
      <PlusOutlined />
      <Amount>
        <InputNumber<string>
          stringMode
          bordered={exchangeStore.direction === ExchangeDirection.TO_FROM}
          min="0.01"
          onChange={handleChangeAmount}
          onFocus={handleFocusAmount}
          size="large"
          step="0.01"
          value={toValue}
        />
      </Amount>
      <CurrencyDropdown
        key={toCurrency}
        defaultValue={toCurrency}
        onChange={handleChangeCurrency}
        options={optionsTo}
      />
      <Limit>
        = {convertNumberToMoney(balanceTo + amountTo)} <span>{toCurrency}</span>
      </Limit>
    </ExchangeRow>
  );
});

export default To;

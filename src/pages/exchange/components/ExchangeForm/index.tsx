import { observer } from "mobx-react-lite";
import From from "pages/exchange/components/ExchangeForm/components/From";
import React, { FC } from "react";
import Swap from "pages/exchange/components/ExchangeForm/components/Swap";
import To from "pages/exchange/components/ExchangeForm/components/To";

const ExchangeForm: FC = observer(() => {
  return (
    <>
      <From />
      <Swap />
      <To />
    </>
  );
});

export default ExchangeForm;

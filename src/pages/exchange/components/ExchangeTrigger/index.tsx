import { Button } from "antd";
import {
  convertNumberToMoney,
  convertNumberToMoneyString,
} from "utils/getFloatFromString";
import { observer } from "mobx-react-lite";
import ExchangeRow from "pages/exchange/components/styled/ExchangeRow";
import React, { FC } from "react";
import useStores from "hooks/useStores";

export interface ExchangeTriggerProps {
  onSubmit: (
    fromCurrency: string,
    toCurrency: string,
    amountFrom: number,
    amountTo: number
  ) => void;
  disabled?: boolean;
}

const ExchangeTrigger: FC<ExchangeTriggerProps> = observer(
  ({ onSubmit, disabled = false }) => {
    const { accountStore, exchangeStore } = useStores();
    const [fromCurrency, toCurrency] = exchangeStore.safePair;
    const balanceFrom = fromCurrency
      ? accountStore.accounts[fromCurrency] ?? 0
      : 0;

    const [amountFrom, amountTo] = exchangeStore.amounts;

    const actualAmountFrom = convertNumberToMoney(amountFrom);
    const actualAmountTo = convertNumberToMoney(amountTo);

    const exchangeNotAllowed =
      disabled ||
      amountFrom > balanceFrom ||
      actualAmountFrom <= 0 ||
      actualAmountTo <= 0 ||
      exchangeStore.minFrom > balanceFrom;

    return (
      <ExchangeRow $gutters="32px 0 32px 32px">
        <Button
          disabled={exchangeNotAllowed}
          onClick={() =>
            onSubmit(fromCurrency, toCurrency, actualAmountFrom, actualAmountTo)
          }
          size="large"
          type="primary"
        >
          Exchange&nbsp;
          <strong>{convertNumberToMoneyString(amountFrom)}</strong>
          <small> {fromCurrency}</small>
          &nbsp;to&nbsp;
          <strong>{convertNumberToMoneyString(amountTo)}</strong>
          <small> {toCurrency}</small>
        </Button>
      </ExchangeRow>
    );
  }
);

export default ExchangeTrigger;

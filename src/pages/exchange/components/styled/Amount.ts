import styled from "styled-components";

const Amount = styled.div`
  .ant-input-number {
    width: 150px;
    margin: 0 0 0 8px;
  }
`;

export default Amount;

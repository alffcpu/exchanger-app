import { SwapOutlined } from "@ant-design/icons";
import styled from "styled-components";

const SwapIcon = styled(SwapOutlined)`
  transform: rotate(90deg) translate(-2px, 1px);
`;

export default SwapIcon;

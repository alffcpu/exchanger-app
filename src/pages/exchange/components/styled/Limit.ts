import styled from "styled-components";

const Limit = styled.div`
  display: flex;
  height: 38px;
  line-height: 38px;
  white-space: nowrap;
  opacity: 0.5;
  padding-left: 16px;
  > span {
    font-size: 80%;
    margin-left: 4px;
    margin-top: 1px;
    display: inline-block;
  }
`;

export default Limit;

import { SyncOutlined } from "@ant-design/icons";
import styled from "styled-components";

const SyncIcon = styled(SyncOutlined)`
  margin-left: 8px;
`;

export default SyncIcon;

import { ExclamationCircleFilled } from "@ant-design/icons";
import styled from "styled-components";

const ErrorIcon = styled(ExclamationCircleFilled)`
  color: #d00;
  margin-left: 8px;
`;

export default ErrorIcon;

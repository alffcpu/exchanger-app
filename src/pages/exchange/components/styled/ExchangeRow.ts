import styled from "styled-components";

const ExchangeRow = styled.div<{ $gutters?: string }>`
  ${({ $gutters = "0" }) => `margin: ${$gutters};`};
  display: flex;
  flex-flow: row nowrap;
  align-items: flex-start;
  > .anticon svg {
    height: 38px;
    font-size: 24px;
  }
  > .ant-select {
    > .ant-select-selector {
      max-height: 39px;
    }
  }
`;

export default ExchangeRow;

import { Select } from "antd";
import styled from "styled-components";

const CurrencyDropdownSelect = styled(Select)`
  min-width: 150px;
`;

export default CurrencyDropdownSelect;

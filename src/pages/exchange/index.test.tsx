import {ExchangeDirection} from "types";
import {exchangeStore} from "stores/ExchangeStore";
import {fireEvent, render, screen} from "@testing-library/react";
import {fxInit} from "utils/money";
import ExchangeForm from "pages/exchange/components/ExchangeForm";
import ExchangeInfo from "pages/exchange/components/ExchangeInfo";
import ExchangeTrigger from "pages/exchange/components/ExchangeTrigger";
import React from "react";
import ratesMock from "mocks/ratesMock";

describe("Exchange", () => {

  test("ExchangeInfo renders with all features", () => {
    fxInit(ratesMock.base, ratesMock.rates);
    exchangeStore.rateUpdated = new Date("2021-03-15");
    exchangeStore.pending = true;
    exchangeStore.ready = true;
    exchangeStore.updateCurrency("EUR", "USD");
    exchangeStore.sourceAmount = 100;
    exchangeStore.setDirection(ExchangeDirection.TO_FROM);
    exchangeStore.sourceAmount = 200;
    const {container, unmount} = render(<ExchangeInfo />);

    expect(container).toMatchSnapshot();

    exchangeStore.setError("Some Error!");
    exchangeStore.setDirection(ExchangeDirection.FROM_TO);
    render(<ExchangeInfo />, {container});

    expect(container).toMatchSnapshot();

    unmount();
  });

  test("ExchangeTrigger renders and handles clicks", () => {

    let sumbitted = {};
    const handleSubmit = jest.fn((...value: any) => {sumbitted = value});
    exchangeStore.setDirection(ExchangeDirection.FROM_TO);
    exchangeStore.updateCurrency("EUR", "USD");
    exchangeStore.calculate("100.50");
    const {container, unmount} = render(<ExchangeTrigger
      onSubmit={handleSubmit}
    />);

    expect(container).toMatchSnapshot();
    const button = screen.getByText(/exchange/i);
    fireEvent.click(button);
    expect(sumbitted).toEqual(['EUR', 'USD', 100.5, 122.53]);

    unmount();
  });

  test("ExchangeForm renders and swap button works", () => {

    exchangeStore.setDirection(ExchangeDirection.TO_FROM);
    exchangeStore.updateCurrency("RUB", "USD");
    exchangeStore.calculate("1000.57");

    const {container, unmount} = render(<ExchangeForm />);
    expect(container).toMatchSnapshot();

    const snap = container.innerHTML;
    const swap = screen.getByTestId("swap-button");
    fireEvent.click(swap);
    render(<ExchangeForm />, {container});
    expect(container.innerHTML).not.toEqual(snap);
    expect(exchangeStore.safePair).toEqual(["USD", "RUB"]);
    unmount();
  });

});

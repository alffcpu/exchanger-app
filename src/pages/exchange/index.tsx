import { Alert, Button, Popover, Spin, message } from "antd";
import { Indicator } from "components/Loading";
import { MESSAGE_DELAY } from "config";
import { observer } from "mobx-react-lite";
import ExchangeForm from "pages/exchange/components/ExchangeForm";
import ExchangeInfo from "pages/exchange/components/ExchangeInfo";
import ExchangeTrigger, {
  ExchangeTriggerProps,
} from "pages/exchange/components/ExchangeTrigger";
import React, { FC, useCallback, useEffect, useState } from "react";
import useRatesData from "pages/exchange/hooks/useRatesData";
import useStores from "hooks/useStores";

const ExchangePage: FC = observer(() => {
  const { accountStore, exchangeStore } = useStores();
  const [exchangeComplete, setExchangeComplete] = useState<string>("");

  useRatesData();

  const [fromCurrency, toCurrency] = exchangeStore.safePair;
  const usedCurrencies = accountStore.usedCurrencies;

  useEffect(() => {
    if (
      !fromCurrency ||
      !usedCurrencies.includes(fromCurrency) ||
      !toCurrency ||
      !usedCurrencies.includes(toCurrency)
    ) {
      exchangeStore.updateCurrency(usedCurrencies[0], usedCurrencies[1]);
    }
  }, [exchangeStore, fromCurrency, toCurrency, usedCurrencies]);

  const handleExchange = useCallback<ExchangeTriggerProps["onSubmit"]>(
    (from, to, amountFrom, amountTo) => {
      const fundsFrom = accountStore.accounts[from];
      const fundsTo = accountStore.accounts[to];
      if (amountFrom > fundsFrom) {
        void message.error("Insufficient funds!", MESSAGE_DELAY);
        return;
      }
      accountStore.update(from, fundsFrom - amountFrom);
      accountStore.update(to, fundsTo + amountTo);
      setExchangeComplete(
        `You have successfully exchanged ${amountFrom} ${from} to ${amountTo} ${to}`
      );
    },
    [accountStore]
  );

  return accountStore.usedCurrencies.length < 2 ? (
    <Alert
      showIcon
      message="Please, open two accounts to use exchange"
      type="warning"
    />
  ) : (
    <Spin
      indicator={<Indicator spin $size={32} />}
      spinning={!exchangeStore.ready}
    >
      <ExchangeForm />
      <Popover
        destroyTooltipOnHide
        content={
          <Button
            onClick={() => setExchangeComplete("")}
            size="middle"
            type="primary"
          >
            Excellent! Let's do it again.
          </Button>
        }
        placement="bottomLeft"
        title={<strong>{exchangeComplete}</strong>}
        visible={!!exchangeComplete}
      >
        <ExchangeTrigger
          disabled={!!exchangeComplete}
          onSubmit={handleExchange}
        />
      </Popover>
      <ExchangeInfo />
    </Spin>
  );
});

export default ExchangePage;

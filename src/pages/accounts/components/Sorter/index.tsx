import { SortDir, SortOrder } from "types";
import { Tag } from "antd";
import { observer } from "mobx-react-lite";
import React, { FC } from "react";
import SortRow, {
  SortBlock,
} from "pages/accounts/components/styled/ExchangeRow";
import useStores from "hooks/useStores";

const { CheckableTag } = Tag;

const orderList: Array<[SortOrder, string]> = [
  [SortOrder.NAME, "Name"],
  [SortOrder.AMOUNT, "Amount"],
];
const dirList: Array<[SortDir, string]> = [
  [SortDir.ASC, "Asc."],
  [SortDir.DESC, "Desc."],
];

const Sorter: FC = observer(() => {
  const { accountStore } = useStores();
  const [order, dir] = accountStore.order;
  return (
    <SortRow>
      <SortBlock>Order by:</SortBlock>
      <SortBlock>
        {orderList.map(([itemOrder, itemTitle]) => (
          <CheckableTag
            key={itemOrder}
            checked={order === itemOrder}
            onChange={() => {
              accountStore.order = [itemOrder, dir];
            }}
          >
            {itemTitle}
          </CheckableTag>
        ))}
      </SortBlock>
      <SortBlock>
        {dirList.map(([itemDir, itemTitle]) => (
          <CheckableTag
            key={itemDir}
            checked={dir === itemDir}
            onChange={() => {
              accountStore.order = [order, itemDir];
            }}
          >
            {itemTitle}
          </CheckableTag>
        ))}
      </SortBlock>
    </SortRow>
  );
});

export default Sorter;

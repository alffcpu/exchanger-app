import styled from "styled-components";

const SortRow = styled.div<{ $gutters?: string }>`
  ${({ $gutters = "16px 0" }) => `margin: ${$gutters};`};
  display: flex;
  flex-flow: row nowrap;
  align-items: flex-start;
  > * {
    margin-right: 16px;
  }
`;

export const SortBlock = styled.div`
  filter: grayscale(1);
  .ant-tag-checkable-checked {
    background-color: #e8e8e8;
    color: #111;
  }
`;

export default SortRow;

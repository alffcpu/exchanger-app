import { Select } from "antd";
import styled from "styled-components";

const CurrencySelect = styled(Select)`
  width: 25%;
  min-width: 150px;
  margin-right: 16px;
`;

export default CurrencySelect;

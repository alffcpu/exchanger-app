import AccountCard from "pages/accounts/components/styled/AccountCard";
import styled from "styled-components";

const AccountsListContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
  margin: 16px -16px 0 0;
  ${AccountCard} {
    margin: 0 16px 16px 0;
  }
`;

export default AccountsListContainer;

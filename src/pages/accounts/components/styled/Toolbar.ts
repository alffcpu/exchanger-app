import { Card } from "antd";
import styled from "styled-components";

const Toolbar = styled(Card)`
  .ant-card-body {
    display: none;
  }
`;

export default Toolbar;

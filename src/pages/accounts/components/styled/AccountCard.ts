import { Card } from "antd";
import styled from "styled-components";

const AccountCard = styled(Card)`
  width: 292px;
  .ant-card-head-wrapper {
    display: flex;
    font-weight: 600;
    button {
      margin-left: 8px;
    }
  }
  .ant-card-body {
    display: flex;
  }
`;

export default AccountCard;

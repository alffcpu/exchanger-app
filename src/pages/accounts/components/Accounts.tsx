import { MESSAGE_DELAY } from "config";
import { message } from "antd";
import { observer } from "mobx-react-lite";
import AccountDetails from "pages/accounts/components/AccountDetails";
import AccountsListContainer from "pages/accounts/components/styled/AccountsListContainer";
import React, { FC } from "react";
import sortAccountsList from "utils/sortAccountsList";
import useStores from "hooks/useStores";

const Accounts: FC = observer(() => {
  const { currencyStore, accountStore } = useStores();

  const currenciesList = currencyStore.currencies ?? {};

  const handleRemove = (currency: string) => {
    accountStore.remove(currency);
    void message.success(
      `«${currency}» account was closed successfully`,
      MESSAGE_DELAY
    );
  };
  const handleUpdate = (currency: string, amount: number) => {
    accountStore.update(currency, amount);
    void message.success(
      `«${currency}» account was successfully updated`,
      MESSAGE_DELAY
    );
  };

  const [order, dir] = accountStore.order;
  const accountsList = sortAccountsList(accountStore.accounts, order, dir);

  return (
    <AccountsListContainer>
      {accountsList.map(([currency, amount]) => (
        <AccountDetails
          key={currency}
          amount={amount}
          currency={currency}
          onRemove={handleRemove}
          onUpdate={handleUpdate}
          title={`${currency} - ${currenciesList[currency]}`}
        />
      ))}
    </AccountsListContainer>
  );
});

export default Accounts;

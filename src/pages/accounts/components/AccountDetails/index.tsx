import { Button, Input, Popconfirm, Tooltip } from "antd";
import { CloseOutlined } from "@ant-design/icons";
import AccountCard from "pages/accounts/components/styled/AccountCard";
import React, { FC, useState } from "react";
import formatFloatString from "utils/formatFloatString";
import getFloatFromString from "utils/getFloatFromString";

interface AccountDetailsProps {
  title: string;
  currency: string;
  amount: number;
  onRemove: (currency: string) => void;
  onUpdate: (currency: string, amount: number) => void;
}

const AccountDetails: FC<AccountDetailsProps> = React.memo(
  ({ title, amount, onRemove, currency, onUpdate }) => {
    const defaultValue = amount.toFixed(2);
    const [updateValue, setUpdateValue] = useState(defaultValue);
    return (
      <AccountCard
        data-testid="account-details"
        extra={
          <Popconfirm
            cancelText="Cancel"
            okText="Close account"
            onConfirm={() => onRemove(currency)}
            placement="top"
            title="Are you sure?"
          >
            <Button
              danger
              data-testid="remove-account"
              size="small"
              type="link">
                <CloseOutlined />
            </Button>
          </Popconfirm>
        }
        title={
          <Tooltip placement="top" title={title}>
            {title}
          </Tooltip>
        }
      >
        <Input
          onBlur={() =>
            updateValue === "" ? setUpdateValue(defaultValue) : null
          }
          onChange={(event) =>
            setUpdateValue(formatFloatString(event?.target?.value ?? ""))
          }
          value={updateValue}
        />
        <Button
          onClick={() => {
            const [newValue, newInputValue] = getFloatFromString(
              updateValue,
              amount
            );
            setUpdateValue(newInputValue);
            onUpdate(currency, newValue);
          }}
          type="primary"
        >
          Update
        </Button>
      </AccountCard>
    );
  }
);

export default AccountDetails;

import { Button, Select } from "antd";
import { CurrenciesList } from "types";
import { PlusOutlined } from "@ant-design/icons";
import CurrencySelect from "pages/accounts/components/styled/CurrencySelect";
import React, { FC, useState } from "react";

interface OpenAccountProps {
  onSubmit: (currency: string) => void;
  currenciesList: CurrenciesList;
  existingAccounts?: string[];
  initialValue?: string;
}

const { Option } = Select;
const OpenAccount: FC<OpenAccountProps> = React.memo(
  ({ onSubmit, existingAccounts = [], currenciesList, initialValue = "" }) => {
    const [currency, setCurrency] = useState(initialValue);
    return (
      <>
        <CurrencySelect
          showSearch
          defaultValue={initialValue}
          filterOption={(input, option) => {
            if (!option) return false;
            const { value } = option;
            const searchContent =
              `${value} ${currenciesList[value]}`.toLowerCase();
            return searchContent.indexOf(input.toLowerCase()) >= 0;
          }}
          onChange={(value) => setCurrency(`${value}`)}
          placeholder="Select a currency"
        >
          {Object.entries(currenciesList).map(([code, title]) => (
            <Option
              key={code}
              disabled={existingAccounts.includes(code)}
              value={code}
            >
              {code} - {title}
            </Option>
          ))}
        </CurrencySelect>
        <Button
          disabled={!currency || existingAccounts.includes(currency)}
          onClick={() => onSubmit(currency)}
          type="primary"
        >
          <PlusOutlined /> Open new account
        </Button>
      </>
    );
  }
);

export default OpenAccount;

import {SortDir, SortOrder} from "types";
import {accountStore} from "stores/AccountStore";
import {fireEvent, render, screen, waitFor} from "@testing-library/react";
import AccountDetails from "pages/accounts/components/AccountDetails";
import OpenAccount from "pages/accounts/components/OpenAccount";
import React from "react";
import Sorter from "pages/accounts/components/Sorter";
import currenciesMock from "mocks/currenciesMock";

describe("Accounts", () => {
  test("AccountDetails renders and handles clicks", async () => {
    const handlers = {
      onRemove: 0,
      onUpdate: 0,
    };
    const handleRemove = jest.fn(() => {handlers.onRemove++});
    const handleUpdate = jest.fn(() => {handlers.onUpdate++});
    const {unmount} = render(<AccountDetails
      amount={987654.99}
      currency="USD"
      onRemove={handleRemove}
      onUpdate={handleUpdate}
      title="USD - Unites States Dollar"
    />);

    const card = screen.getByTestId("account-details");
    expect(card).toMatchSnapshot();

    const update = screen.getByText("Update");
    fireEvent.click(update);
    const remove = screen.getByTestId("remove-account");
    fireEvent.click(remove);

    const closeText = "Close account";
    await waitFor(() => screen.getByText(closeText));

    const cancel = screen.getByText("Cancel");
    fireEvent.click(cancel);
    fireEvent.click(remove);

    await waitFor(() => screen.getByText(closeText));
    const confirm = screen.getByText(closeText);
    fireEvent.click(confirm);

    expect(handlers).toEqual({
      onRemove: 1,
      onUpdate: 1,
    });

    unmount();
  });

  test("OpenAccount renders disabled", () => {
    const handleSubmit = jest.fn();
    const {unmount, container} = render(<OpenAccount
      currenciesList={currenciesMock}
      existingAccounts={[]}
      onSubmit={handleSubmit}
    />);

    expect(container).toMatchSnapshot();
    unmount();
  });

  test("OpenAccount renders enabled and handles click", async () => {

    let submitted = false;
    const handleSubmit = jest.fn(() => {submitted = true});
    const {unmount, container} = render(<OpenAccount
      currenciesList={currenciesMock}
      existingAccounts={[]}
      initialValue="USD"
      onSubmit={handleSubmit}
    />);

    expect(container).toMatchSnapshot();
    fireEvent.click(screen.getByText(/open new account/i));

    expect(submitted).toBeTruthy();

    unmount();
  });

  test("Sorter renders", () => {
    accountStore.order = [SortOrder.NAME, SortDir.ASC];
    const {unmount, container} = render(<Sorter />);

    expect(container).toMatchSnapshot();

    const amount = screen.getByText(/amount/i);
    fireEvent.click(amount);
    const desc = screen.getByText(/desc/i);
    fireEvent.click(desc);
    expect(accountStore.order).toEqual([SortOrder.AMOUNT, SortDir.DESC]);

    const name = screen.getByText(/name/i);
    fireEvent.click(name);
    const asc = screen.getByText(/asc/i);
    fireEvent.click(asc);
    expect(accountStore.order).toEqual([SortOrder.NAME, SortDir.ASC]);

    unmount();
  });

});

import { DEFAULT_ACCOUNT_BALANCE, MESSAGE_DELAY } from "config";
import { message } from "antd";
import { observer } from "mobx-react-lite";
import Accounts from "pages/accounts/components/Accounts";
import OpenAccount from "pages/accounts/components/OpenAccount";
import React, { FC } from "react";
import Sorter from "pages/accounts/components/Sorter";
import Toolbar from "pages/accounts/components/styled/Toolbar";
import useStores from "hooks/useStores";

const AccountsPage: FC = observer(() => {
  const { currencyStore, accountStore } = useStores();
  const currenciesList = currencyStore.currencies ?? {};

  const handleAccountCreate = (currency: string) => {
    accountStore.update(currency, DEFAULT_ACCOUNT_BALANCE);
    void message.success(
      `Account was successfully created with initial deposit of ${DEFAULT_ACCOUNT_BALANCE} ${currency.toLowerCase()}.`,
      MESSAGE_DELAY
    );
  };

  return (
    <>
      <Toolbar
        size="small"
        title={
          <OpenAccount
            currenciesList={currenciesList}
            existingAccounts={accountStore.usedCurrencies}
            onSubmit={handleAccountCreate}
          />
        }
      />
      <Sorter />
      <Accounts />
    </>
  );
});

export default AccountsPage;

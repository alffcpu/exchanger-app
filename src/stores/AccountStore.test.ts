import {DEFAULT_ACCOUNT_BALANCE} from "config";
import {SortDir, SortOrder} from "types";
import {accountStore} from "stores/AccountStore";

describe("AccountStore", () => {
  test("should works correctly with default settings", () => {
    expect(accountStore.order).toEqual([accountStore.orderOrder, accountStore.orderDir]);
    accountStore.order = [SortOrder.AMOUNT, SortDir.DESC];
    expect(accountStore.order).toEqual([SortOrder.AMOUNT, SortDir.DESC]);
    expect(accountStore.usedCurrencies).toEqual(Object.keys(accountStore.accounts));
    accountStore.update("PHP", DEFAULT_ACCOUNT_BALANCE - 100);
    expect(accountStore.accounts["PHP"]).toEqual(DEFAULT_ACCOUNT_BALANCE - 100);
    accountStore.remove("PHP");
    expect(accountStore.accounts["PHP"]).toBeUndefined();
  });
});

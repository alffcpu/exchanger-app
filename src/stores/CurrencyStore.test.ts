import {currencyStore} from "stores/CurrencyStore";
import {rest} from "msw";
import {setupServer} from "msw/node";
import currenciesMock from "mocks/currenciesMock";
import getApiUrl, {APIMethods} from "api/getApiUrl";

const urlCurrencies = getApiUrl(APIMethods.GET_CURRENCIES_LIST).split("?")[0];
let successResponse = true;
const server = setupServer(
  rest.get(urlCurrencies, (req, res, ctx) => {
    return successResponse ?
      res(ctx.json(currenciesMock)) :
      res(
        ctx.status(500, "Network error"),
        ctx.body("")
      );
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("CurrencyStore", () => {
  test("should works correctly with default settings", async () => {

    expect(currencyStore.isError).toBeFalsy();
    await currencyStore.getCurrenciesList();
    expect(currencyStore.currencies).toEqual(currenciesMock);
    expect(currencyStore.isReady).toBeTruthy();
    successResponse = false;
    currencyStore.setCurrencies(null);
    await currencyStore.getCurrenciesList();
    expect(currencyStore.isError).toBeTruthy();
  });
});

import { accountStore } from "stores/AccountStore";
import { create } from "mobx-persist";
import { currencyStore } from "stores/CurrencyStore";
import { exchangeStore } from "stores/ExchangeStore";
import React from "react";

export const stores = Object.freeze({
  accountStore,
  currencyStore,
  exchangeStore,
});

const hydrate = create({});
hydrate("accountStore", accountStore);
hydrate("exchangeStore", exchangeStore);

export const storesContext = React.createContext(stores);
export const StoresProvider = storesContext.Provider;

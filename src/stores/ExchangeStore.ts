import { ExchangeDirection } from "types";
import { action, makeAutoObservable } from "mobx";
import { fxConvert, fxInit, getRate } from "utils/money";
import { persist } from "mobx-persist";
import fetchRates from "../api/fetchRates";
import fetchRatesUpdate from "api/fetchRatesUpdate";
import getFloatFromString, {
  convertNumberToMoney,
} from "utils/getFloatFromString";

export default class ExchangeStore {
  constructor() {
    makeAutoObservable(this);
  }

  @persist
  public from: string | null = "";

  @persist
  public to: string | null = "";

  @persist
  public amountFrom = 10;

  @persist
  public amountTo = 0;

  public direction: ExchangeDirection = ExchangeDirection.FROM_TO;

  public rateLastUpdate: Date | null = null;

  public rateReady = false;
  public ratePending = false;
  public error: string | null = null;

  public get rate(): number {
    return this.ready && this.rateUpdated != null
      ? getRate(this.from, this.to)
      : 0;
  }

  public get crossRate(): number {
    return this.ready && this.rateUpdated != null
      ? getRate(this.to, this.from)
      : 0;
  }

  public get sourceAmount() {
    return this.direction === ExchangeDirection.FROM_TO
      ? this.amountFrom
      : this.amountTo;
  }

  public set sourceAmount(value: number) {
    if (this.direction === ExchangeDirection.FROM_TO) {
      this.amountFrom = value;
    } else {
      this.amountTo = value;
    }
  }

  public get rateUpdated() {
    return this.rateLastUpdate;
  }

  public set rateUpdated(date: Date | null) {
    this.rateLastUpdate = date;
  }

  public get safePair(): [string, string] {
    return [this.from ?? "", this.to ?? ""];
  }

  public get amounts(): [number, number] {
    return [this.amountFrom, this.amountTo];
  }

  public get minFrom(): number {
    return this.crossRate > this.rate
      ? convertNumberToMoney(this.crossRate / 100)
      : 0.01;
  }

  public get ready() {
    return this.rateReady;
  }

  public set ready(state: boolean) {
    this.rateReady = state;
  }

  public get pending() {
    return this.ratePending;
  }

  public set pending(state: boolean) {
    this.ratePending = state;
  }

  @action
  public recalculate = () => {
    this.calculate(`${this.sourceAmount}`);
  };

  @action
  public calculate = (value?: string | null) => {
    if (value == null) return;
    const [amount] = getFloatFromString(value, this.sourceAmount);
    if (!isNaN(amount)) {
      const isFromTo = this.direction === ExchangeDirection.FROM_TO;
      this.sourceAmount = amount;

      const result = isFromTo
        ? fxConvert(this.from, this.to, amount)
        : fxConvert(this.to, this.from, amount);

      const minResult = !result && amount > 0 ? 0.01 : result;
      if (isFromTo) {
        this.amountTo = minResult;
      } else {
        this.amountFrom = minResult;
      }
    }
  };

  @action
  public updateCurrency = (from?: string | null, to?: string | null) => {
    if (from != null) {
      this.from = from;
    }
    if (to != null) {
      this.to = to;
    }
    this.recalculate();
  };

  @action
  public setFrom = (from: string) => {
    this.updateCurrency(from);
  };

  @action
  public setTo = (to: string) => {
    this.updateCurrency(null, to);
  };

  @action
  public refresh = async (simulate = this.ready) => {
    this.pending = true;
    this.setError(null);
    try {
      const handler = simulate ? fetchRatesUpdate : fetchRates;
      const { base, rates } = await handler();
      fxInit(base, rates);
      this.rateUpdated = new Date();
      this.ready = true;
    } catch (error) {
      this.setError(`${error}`);
    } finally {
      this.pending = false;
    }
  };

  @action
  public setError(error: string | null) {
    this.error = error;
  }

  @action
  public setDirection = (direction: ExchangeDirection) => {
    this.direction = direction;
  };

  @action
  public setDirectionFromTo = () => {
    this.setDirection(ExchangeDirection.FROM_TO);
  };

  @action
  public setDirectionToFrom = () => {
    this.setDirection(ExchangeDirection.TO_FROM);
  };
}

export const exchangeStore = new ExchangeStore();

import getApiUrl, {APIMethods} from "api/getApiUrl";
import ratesMock from "mocks/ratesMock";
import {rest} from "msw";
import {setupServer} from "msw/node";
import {exchangeStore} from "stores/ExchangeStore";
import {ExchangeDirection} from "types";

const urlRates = getApiUrl(APIMethods.GET_RATES).split("?")[0];
const server = setupServer(
  rest.get(urlRates, (req, res, ctx) => {
    return res(ctx.json(ratesMock));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("ExchangeStore", () => {
  test("should works correctly with default settings", async () => {

    await exchangeStore.refresh();
    expect(exchangeStore.rateReady).toBeTruthy();
    exchangeStore.updateCurrency("USD", "RUB");
    const rate = exchangeStore.rate;
    const crossRate = exchangeStore.crossRate;
    expect(rate).toEqual(73.2009);
    expect(crossRate).toEqual(0.01366103422225683);
    expect(exchangeStore.sourceAmount).toEqual(exchangeStore.amountFrom);
    exchangeStore.setDirection(ExchangeDirection.TO_FROM);
    expect(exchangeStore.sourceAmount).toEqual(exchangeStore.amountTo);
    exchangeStore.sourceAmount = 999;
    expect(exchangeStore.amountTo).toEqual(999);
    expect(exchangeStore.safePair).toEqual(["USD", "RUB"]);
    expect(exchangeStore.amounts).toEqual([10, 999]);
    exchangeStore.updateCurrency("RUB", "USD");
    expect(exchangeStore.minFrom).toEqual(0.73);
    exchangeStore.setDirection(ExchangeDirection.FROM_TO);
    exchangeStore.calculate("10000");
    expect(exchangeStore.amountTo).toEqual(136.6103422225683);
    await exchangeStore.refresh();
    exchangeStore.updateCurrency("USD", "RUB");
    expect(exchangeStore.crossRate).not.toEqual(crossRate);

  });
});

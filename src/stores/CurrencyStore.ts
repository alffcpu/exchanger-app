import { CurrenciesList } from "types";
import { action, makeAutoObservable } from "mobx";
import fetchCurrenciesList from "api/fetchCurrenciesList";

export default class CurrencyStore {
  constructor() {
    makeAutoObservable(this);
  }

  public pending = false;
  public error: string | null = null;
  public currencies: CurrenciesList | null = null;

  public get isReady() {
    return !!this.currencies;
  }

  public get isPending() {
    return !this.isReady && this.pending;
  }

  public get isError() {
    return !this.currencies && this.error;
  }

  @action
  public getCurrenciesList = async () => {
    this.setPending(true);
    this.setError(null);
    try {
      const currencies = await fetchCurrenciesList();
      this.setCurrencies(currencies);
    } catch (error) {
      this.setError(`${error}`);
    } finally {
      this.setPending(false);
    }
  };

  @action
  public setCurrencies = (currencies: CurrenciesList | null) => {
    this.currencies = currencies;
  };

  @action
  public setPending = (pending: boolean) => {
    this.pending = pending;
  };

  @action
  public setError = (error: string | null) => {
    this.error = error;
  };
}

export const currencyStore = new CurrencyStore();

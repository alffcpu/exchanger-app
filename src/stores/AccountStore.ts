import { AccountsList, SortDir, SortOrder } from "types";
import { DEFAULT_ACCOUNT_BALANCE } from "config";
import { makeAutoObservable } from "mobx";
import { persist } from "mobx-persist";

export default class AccountStore {
  constructor() {
    makeAutoObservable(this);
  }

  @persist("object")
  public accounts: AccountsList = {
    EUR: DEFAULT_ACCOUNT_BALANCE,
    GBP: DEFAULT_ACCOUNT_BALANCE,
    RUB: DEFAULT_ACCOUNT_BALANCE,
    USD: DEFAULT_ACCOUNT_BALANCE,
  };

  @persist
  public orderOrder: SortOrder = SortOrder.NAME;

  @persist
  public orderDir: SortDir = SortDir.ASC;

  public get order(): [SortOrder, SortDir] {
    return [this.orderOrder, this.orderDir];
  }

  public set order([order, dir]: [SortOrder, SortDir]) {
    this.orderOrder = order;
    this.orderDir = dir;
  }

  public get usedCurrencies() {
    return Object.keys(this.accounts);
  }

  public update = (
    currency: string,
    amount: number = DEFAULT_ACCOUNT_BALANCE
  ) => {
    this.accounts[currency] = amount;
  };

  public remove = (currencyCode: string) => {
    this.accounts = Object.entries(this.accounts).reduce<AccountsList>(
      (result, [code, amount]) =>
        code !== currencyCode
          ? {
              ...result,
              [code]: amount,
            }
          : result,
      {}
    );
  };
}

export const accountStore = new AccountStore();

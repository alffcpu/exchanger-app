import { Route, Switch } from "react-router-dom";
import { observer } from "mobx-react-lite";
import AccountsPage from "pages/accounts";
import Error404Page from "pages/Error404Page";
import ExchangePage from "pages/exchange";
import React, { FC } from "react";
import getPath from "utils/getPath";

const Navigation: FC = observer(() => {
  return (
    <Switch>
      <Route exact component={AccountsPage} path={[getPath(""), getPath("accounts")]} />
      <Route exact component={ExchangePage} path={getPath("exchange")} />
      <Route component={Error404Page} />
    </Switch>
  );
});

export default Navigation;

import { render, screen } from "@testing-library/react";
import Loading from "components/Loading";
import React from "react";

describe("Loading ", () => {
  test("should render with icon", () => {
    render(<Loading/>);
    const preloader = screen.getByTestId("common-preloader");
    expect(preloader).toBeInTheDocument();
    const icon = screen.getByTestId("common-preloader-icon");
    expect(icon).toBeInTheDocument();
  });
});


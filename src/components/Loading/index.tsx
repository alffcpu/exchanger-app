import { LoadingOutlined } from "@ant-design/icons";
import { Spin } from "antd";
import React, { FC } from "react";
import styled from "styled-components";

interface LoadingProps {
  size?: number;
}

export const Indicator = styled(LoadingOutlined)<{ $size: number }>`
  ${({ $size }) => `
  font-size: ${$size}px;
`}
`;

const Loading: FC<LoadingProps> = ({ size = 32 }) => {
  return (
    <Spin
      data-testid="common-preloader"
      indicator={<Indicator
        spin
        $size={size}
        data-testid="common-preloader-icon"
      />}
      tip="Please, wait..."
    />
  );
};

export default Loading;

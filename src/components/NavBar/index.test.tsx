import {fireEvent, render, screen} from "@testing-library/react";
import NavBar from "components/NavBar";
import React from "react";

describe("Navbar", () => {
  test("should render and have two tabs", () => {

    render(<NavBar activeKey="accounts" onChange={() => null}/>);
    const navBar = screen.getByTestId("navigation-bar");
    expect(navBar).toMatchSnapshot();
    const tabs = screen.getAllByRole("tab");
    expect(tabs.length).toBe(2);
  });

  test("should onChange switch tabs", () => {
    let activeKey = "exchange";
    const handleChange = jest.fn((value: string) => {activeKey = value});
    const {container} = render(<NavBar activeKey={activeKey} onChange={handleChange}/>);
    const initiallyActiveTab = container.querySelector(".ant-tabs-tab-active");
    expect(initiallyActiveTab).not.toBe(null);
    expect(initiallyActiveTab?.textContent?.toLowerCase()).toBe(activeKey);
    const tab = container.querySelector(".ant-tabs-tab:nth-child(1)");
    if (tab) {
      fireEvent.click(tab);
    }
    expect(handleChange).toHaveBeenCalledTimes(1);
    render(
      <NavBar activeKey={activeKey} onChange={handleChange}/>,
      {container}
    );
    expect(activeKey).toBe("accounts");
    const activeTab = container.querySelector(".ant-tabs-tab-active");
    expect(activeTab).not.toBe(null);
    expect(activeTab?.textContent?.toLowerCase()).toBe("accounts");
  });

});


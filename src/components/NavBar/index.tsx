import { ReloadOutlined, WalletOutlined } from "@ant-design/icons";
import { Tabs } from "antd";
import React, { FC } from "react";

interface NavBarProps {
  onChange: (key: string) => void;
  activeKey?: string;
}

const { TabPane } = Tabs;

const NavBar: FC<NavBarProps> = ({ activeKey, onChange }) => {
  return (
    <Tabs
      data-testid="navigation-bar"
      defaultActiveKey={activeKey}
      onChange={onChange}
    >
      <TabPane
        key="accounts"
        tab={
          <>
            <WalletOutlined />
            ACCOUNTS
          </>
        }
      />
      <TabPane
        key="exchange"
        tab={
          <>
            <ReloadOutlined />
            EXCHANGE
          </>
        }
      />
    </Tabs>
  );
};

export default NavBar;

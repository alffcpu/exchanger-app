import { render, screen } from "@testing-library/react";
import Page from "components/Page";
import React from "react";

describe("Page", () => {
  test("should render", () => {
    render(<Page>
      <h1>All your bases are belong to us!</h1>
    </Page>);
    const page = screen.getByTestId("layout-page");
    expect(page).toMatchSnapshot();
  });
});


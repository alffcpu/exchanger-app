import Layout from "components/Page/styled/Layout";
import PageContent from "components/Page/styled/PageContent";
import React, { FC } from "react";

const Page: FC = ({ children }) => {
  return (
    <Layout data-testid="layout-page">
      <PageContent>{children}</PageContent>
    </Layout>
  );
};

export default Page;

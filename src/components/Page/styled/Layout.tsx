import { Layout as AntLayout } from "antd";
import styled from "styled-components";

const Layout = styled(AntLayout)`
  min-height: calc(100vh - 0px);
`;

export default Layout;

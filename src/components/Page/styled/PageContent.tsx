import { Layout } from "antd";
import styled from "styled-components";

const PageContent = styled(Layout.Content)`
  width: 100%;
  max-width: 1280px;
  margin: 5vh auto;
  padding: 16px 32px 32px 32px;
  background-color: #fff;
`;

export default PageContent;

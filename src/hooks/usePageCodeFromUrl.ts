import { useLocation } from "react-router-dom";
import trim from "lodash/trim";

const usePageCodeFromUrl = (): string => {
  const { pathname } = useLocation();
  return trim(pathname.substr(process.env.PUBLIC_URL.length), "/").split(
    "/"
  )[0];
};

export default usePageCodeFromUrl;

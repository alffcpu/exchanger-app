import { CurrencyOptionItem } from "types";
import { useMemo } from "react";
import getCurrencyOptionsList from "utils/getCurrencyOptionsList";
import useStores from "hooks/useStores";

const useCurrencyOptionsList = (
  disabledCurrency: string | null
): CurrencyOptionItem[] => {
  const { accountStore, currencyStore } = useStores();
  const currenciesList = currencyStore.currencies;
  const usedCurrencies = accountStore.usedCurrencies;
  return useMemo(
    () =>
      getCurrencyOptionsList(currenciesList, usedCurrencies, disabledCurrency),
    [currenciesList, disabledCurrency, usedCurrencies]
  );
};

export default useCurrencyOptionsList;

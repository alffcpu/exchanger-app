import { storesContext } from "stores";
import { useContext } from "react";

export const useStores = () => useContext(storesContext);

export default useStores;

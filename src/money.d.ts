declare module "money" {
  let base: string;
  let rates: Record<string, number>;
  let settings: Record<string, string>;
  const convert = (number) => number;
}

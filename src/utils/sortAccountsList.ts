import {AccountsList, SortDir, SortOrder} from "types";
import orderBy from "lodash/orderBy";

const sortAccountsList = (accounts: AccountsList, order: SortOrder, dir: SortDir) => orderBy(
  Object.entries(accounts),
  order === SortOrder.NAME ? (value) => value[0] : (value) => value[1],
  dir === SortDir.ASC ? "asc" : "desc"
);

export default sortAccountsList;

import { CurrenciesList, CurrencyOptionItem } from "types";

const getCurrencyOptionsList = (
  currenciesList: CurrenciesList | null,
  usedCurrencies: string[],
  disabledCurrency: string | null
): CurrencyOptionItem[] => {
  return currenciesList
    ? usedCurrencies.map<CurrencyOptionItem>((currency) => ({
        value: currency,
        title: `${currency} - ${currenciesList[currency]}`,
        disabled: currency === disabledCurrency,
      }))
    : [];
};

export default getCurrencyOptionsList;

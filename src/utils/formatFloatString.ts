const formatFloatString = (value: string) => {
  const stripedValue = typeof value !== "string" ?
    "" :
    value.replace(/[^\d.]/g, "");
  const newValue = stripedValue.replace(
    /^([^.]*\.)(.*)$/,
    (a: string, b: string, c: string) => b + c.replace(/\./g, "")
  );
  return newValue[0] === "." ? `0${newValue}` : newValue;
};

export default formatFloatString;

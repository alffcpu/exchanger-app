import { Rates } from "types";
import fx from "money";

export const fxConvert = (
  from?: string | null,
  to?: string | null,
  amount?: number
): number => {
  if (!from || !to || !amount || !Object.keys(fx.rates).length || !fx.base)
    return 0;
  fx.settings = { from, to };
  return fx.convert(amount);
};

export const fxInit = (base: string, rates: Rates): void => {
  fx.base = base;
  fx.rates = rates;
};

export const fxRates = (): Rates => fx.rates ?? {};

export const fxBase = (): string => fx.base ?? "USD";

export const getRate = (from?: string | null, to?: string | null): number => {
  if (!from || !to) return 0;
  return fxConvert(from, to, 1);
};

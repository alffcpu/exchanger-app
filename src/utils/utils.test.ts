import {SortDir, SortOrder} from "types";
import {fxBase, fxConvert, fxInit, fxRates, getRate} from "utils/money";
import currenciesMock from "mocks/currenciesMock";
import formatFloatString from "utils/formatFloatString";
import getCurrencyOptionsList from "utils/getCurrencyOptionsList";
import getFloatFromString, {
  convertNumberToLongMoneyString,
  convertNumberToMoney,
  convertNumberToMoneyString
} from "utils/getFloatFromString";
import ratesMock from "mocks/ratesMock";
import sleep from "utils/sleep";
import getPath from "utils/getPath";
import sortAccountsList from "utils/sortAccountsList";

describe("utils ", () => {
  test("formatFloatString should works correctly", () => {
    const res1 = formatFloatString("123.20");
    expect(res1).toEqual("123.20");
    const res2 = formatFloatString(".99");
    expect(res2).toEqual("0.99");
    const res3 = formatFloatString("abcdef90.1zzz");
    expect(res3).toEqual("90.1");
    const res4 = formatFloatString(null as any);
    expect(res4).toEqual("");
  });

  test("getCurrencyOptionsList should works correctly", () => {
    const allOpts = getCurrencyOptionsList(currenciesMock, Object.keys(currenciesMock), null);
    expect(allOpts.length).toEqual(Object.keys(currenciesMock).length);
    const usedCurrencies = ["USD", "EUR", "RUR"];
    const withoutSome =  getCurrencyOptionsList(currenciesMock, usedCurrencies, null);
    const allLabels = withoutSome.reduce<string>((r, {value}) => `${r}${value}`, "");
    expect(allLabels).toEqual(usedCurrencies.join(""));
    const withDisabledUsd = getCurrencyOptionsList(currenciesMock, usedCurrencies, "USD");
    expect(withDisabledUsd[0].disabled).toBeTruthy();
  });

  test("getFloatFromString should works correctly", () => {
    const res1 = getFloatFromString("123.12", 0);
    expect(res1).toEqual([123.12, "123.12"]);
    const res2 = getFloatFromString("-234.44", 0);
    expect(res2).toEqual([-234.44, "-234.44"]);
    const res3 = getFloatFromString("zz234ddd.dff44ff", 0);
    expect(res3).toEqual([0, "0.00"]);
    const res4 = getFloatFromString(null as any, 999);
    expect(res4).toEqual([999, "999.00"]);
  });

  test("convertNumberToMoneyString should works correctly", () => {
    const res1 = convertNumberToMoneyString(999.998);
    expect(res1).toEqual("1000.00");
    const res2 = convertNumberToMoneyString(55.554);
    expect(res2).toEqual("55.55");
    const res3 = convertNumberToMoneyString(1);
    expect(res3).toEqual("1.00");
    const res4 = convertNumberToMoneyString(null as any);
    expect(res4).toEqual("0.00");
  });

  test("convertNumberToMoney should works correctly", () => {
    const res1 = convertNumberToMoney(999.998);
    expect(res1).toEqual(1000.00);
    const res2 = convertNumberToMoney(2);
    expect(res2).toEqual(2);
    const res3 = convertNumberToMoney(1.011);
    expect(res3).toEqual(1.01);
    const res4 = convertNumberToMoney(null as any);
    expect(res4).toEqual(0);
  });

  test("convertNumberToLongMoneyString should works correctly", () => {
    const res1 = convertNumberToLongMoneyString(999.9999998);
    expect(res1).toEqual("1000.000000");
    const res2 = convertNumberToLongMoneyString(3);
    expect(res2).toEqual("3.000000");
    const res3 = convertNumberToLongMoneyString(99.9999992);
    expect(res3).toEqual("99.999999");
    const res4 = convertNumberToLongMoneyString(null as any);
    expect(res4).toEqual("0.000000");
  });

  test("money.js wrappers should work correctly", () => {
    fxInit(ratesMock.base, ratesMock.rates);
    expect(fxBase()).toEqual(ratesMock.base);
    expect(fxRates()).toEqual(ratesMock.rates);
    const crossRub = getRate("USD", "RUB");
    expect(crossRub).toEqual(73.2009);
    const convResult = fxConvert("USD", "RUB", 100);
    expect(convResult).toEqual(7320.09);
    expect(() => fxConvert("ZZZ", "NNN", 5)).toThrow("fx error");
  });

  test("sleep should works correctly", async () => {
    const now = new Date().getTime();
    await sleep(250);
    const futureNow = new Date().getTime();
    expect(futureNow - now).toBeGreaterThanOrEqual(250);
    const now2 = new Date().getTime();
    sleep(250);
    const futureNow2 = new Date().getTime();
    expect(futureNow2 - now2).toBeLessThan(250);
  });

  test("sortAccountsList should works correctly", () => {
    const accounts = {
      USD: 100,
      EUR: 300,
      GBP: 400,
      RUB: 200,
    };
    const getKeys = (sortedAccounts: Array<[string, number]>) => sortedAccounts.reduce<string>((result, [key]) => `${result}${key}`, "");
    const sortedByNameAsc = sortAccountsList(accounts, SortOrder.NAME, SortDir.ASC);
    expect(getKeys(sortedByNameAsc)).toEqual("EURGBPRUBUSD");
    const sortedByNameDesc = sortAccountsList(accounts, SortOrder.NAME, SortDir.DESC);
    expect(getKeys(sortedByNameDesc)).toEqual("USDRUBGBPEUR");
    const sortedByAmountAsc = sortAccountsList(accounts, SortOrder.AMOUNT, SortDir.ASC);
    expect(getKeys(sortedByAmountAsc)).toEqual("USDRUBEURGBP");
    const sortedByAmountDesc = sortAccountsList(accounts, SortOrder.AMOUNT, SortDir.DESC);
    expect(getKeys(sortedByAmountDesc)).toEqual("GBPEURRUBUSD");
  });

  test("getPath should works correctly", () => {
    expect(getPath("account")).toEqual(`${process.env.PUBLIC_URL}/account`);
    expect(getPath(null as any)).toEqual(`${process.env.PUBLIC_URL}/null`);
  });
});

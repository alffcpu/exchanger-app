const getFloatFromString = (
  sourceValue: string,
  defaultValue: number
): [number, string] => {
  const value = parseFloat(sourceValue);
  const newValue = isNaN(value) ? defaultValue : value;
  const newStrValue = convertNumberToMoneyString(newValue);
  return [parseFloat(newStrValue), newStrValue];
};

export const convertNumberToMoneyString = (num: number): string =>
  (Math.round(num * 100) / 100).toFixed(2);

export const convertNumberToMoney = (num: number): number =>
  parseFloat(convertNumberToMoneyString(num));

export const convertNumberToLongMoneyString = (num: number): string =>
  (Math.round(num * 1000000) / 1000000).toFixed(6);

export default getFloatFromString;

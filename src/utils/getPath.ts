const getPath = (path: string): string => `${process.env.PUBLIC_URL}/${path}`;

export default getPath;

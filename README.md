# Exchange app

The task is [here](http://dev.thesuper.ru/FrontendDevelopmentHomeTask-Revolut.docx).  
  
__DEMO IS HERE: http://dev.thesuper.ru/exchange-app/__  
  
## Please, note!  
  
OXR free api has a limit of request per day and it diesn't update often, that's why rates refresh and live updates are simulated. 
After simulated call rates are just slightly alter in comparision with that received on first response. So, after 10s. update exchange rates might look weird. 
It was done intentionally to check if live update works.  
  
## Available Scripts

In the project directory, you can run:

### `yarn install`

Installs the dependencies.  

### `yarn start`

Runs the app in the development mode.  

### `yarn test`

Launches the test runner in the interactive watch mode.  

### `yarn build`

Builds the app for production to the `build` folder.  
